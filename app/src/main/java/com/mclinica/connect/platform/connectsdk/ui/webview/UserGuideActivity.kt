package com.mclinica.connect.platform.connectsdk.ui.webview

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.mclinica.connect.platform.connectsdk.R
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import com.mclinica.connect.platform.sdk.ui.WebViewFragment
import kotlinx.android.synthetic.main.activity_user_guide.*

class UserGuideActivity : AppCompatActivity() {

    lateinit var fragment: WebViewFragment

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, UserGuideActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_guide)

        setSupportActionBar(include_toolbar as Toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_back)
        title = resources.getString(R.string.activity_webview_title)

        if (savedInstanceState == null) {
            fragment = WebViewFragment.newInstance(WebViewFragment.TYPE_FAQ)
            supportFragmentManager.beginTransaction().replace(R.id.lin_container, fragment).commit()
        }
    }

    override fun onResume() {
        super.onResume()
        fragment.loadWebView({
            AppLog.e("web view successfully loaded")
        }, {
            AppLog.e("web view failed to load")
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
