package com.mclinica.connect.platform.connectsdk.ui.login

import android.content.Intent
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.mclinica.connect.platform.connectsdk.SDKApplication
import com.mclinica.connect.platform.connectsdk.data.AppDatabaseManager
import com.mclinica.connect.platform.sdk.ConnectSDK.context
import com.mclinica.connect.platform.sdk.auth.Auth
import com.mclinica.connect.platform.sdk.data.AuthAPIManager
import com.mclinica.connect.platform.sdk.data.ConnectAPIManager
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.helper.callback.FailCallback
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallback
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import net.openid.appauth.AuthorizationException
import net.openid.appauth.AuthorizationResponse
import javax.inject.Inject

class LoginViewModel : ViewModel() {

    @Inject
    lateinit var appDatabaseManager: AppDatabaseManager

    @Inject
    lateinit var connectAPIManager: ConnectAPIManager

    @Inject
    lateinit var authAPIManager: AuthAPIManager

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        SDKApplication.appComponent.inject(this)
    }

    //for keycloak login
    fun login(fragment: Fragment, fail: FailCallback) {
        Auth.auth().connect(fragment, {
            AppLog.e(it?.localizedMessage.toString())
            fail.invoke()
        })
    }

    //for manual login
    fun manualLogin(
        username: EditText,
        password: EditText,
        success: SuccessCallback,
        fail: FailCallback
    ) {

        if (username.text.toString().isNullOrBlank()) {
            username.error = "This field is required."
            username.requestFocus()
            return
        } else {
            username.error = null
        }

        if (password.text.toString().isNullOrBlank()) {
            password.error = "This field is required."
            password.requestFocus()
            return
        } else {
            password.error = null
        }

        //get access token and store user credentials
        val getAccessToken =
            authAPIManager.getAuthToken(username.text.toString(), password.text.toString())
                .subscribe({ authToken ->
                    LocalDisk.authToken = authToken
                    LocalDisk.accessGrantType = LocalDisk.GRANT_TYPE_PASSWORD
                    LocalDisk.username = username.text.toString()
                    LocalDisk.password = password.text.toString()
                    LocalDisk.isLoggedIn = true
                    success.invoke()
                }, {
                    AppLog.e("error: ${it.message.toString()}")
                    fail.invoke()
                })

        compositeDisposable.add(getAccessToken)

    }

    //this is called every time app is opened
    fun checkAppVersion(success: SuccessCallback, fail: FailCallback) {
        val getVersion = connectAPIManager.getVersion()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ appVersion ->

                val oldConfigHash = LocalDisk.configHash //from local
                val newConfigHash = appVersion.result.hash //from api

                if (oldConfigHash.isNullOrEmpty()) {
                    //no config set, saving new config and save new config has
                    AppLog.e("no config set, saving new config")
                    LocalDisk.configHash = newConfigHash
                    getAppConfigFromAPI(success, fail)
                } else {
                    if (oldConfigHash != newConfigHash) {
                        //update config in local db and save new config has
                        AppLog.e("update config in local db")
                        LocalDisk.configHash = newConfigHash
                        getAppConfigFromAPI(success, fail)
                    } else {
                        //no config changes, using config from local db
                        AppLog.e("no config changes, using config from local db")
                        Auth.auth(
                            context,
                            LocalDisk.appConfiguration?.keycloakConfig!!.authConfig()
                        )
                        success.invoke()
                    }
                }

            }, {
                fail.invoke()
            })
        compositeDisposable.add(getVersion)
    }

    private fun getAppConfigFromAPI(success: SuccessCallback, fail: FailCallback) {
        val getConfigFromAPI = connectAPIManager.getConfigFromAPI()
            .map { config ->
                appDatabaseManager.deleteConfig()
                appDatabaseManager.saveConfig(config.keycloakConfig)
                LocalDisk.appConfiguration = config
                Auth.auth(context, config.keycloakConfig.authConfig())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                success.invoke()
            }, {
                AppLog.e("error: ${it.message.toString()}")
                fail.invoke()
            })
        compositeDisposable.add(getConfigFromAPI)
    }

    //get profile when using manual login
    fun getProfile(success: SuccessCallback, fail: FailCallback) {
        getProfile(null, success, fail)
    }

    //get profile when using keycloak
    fun getProfile(data: Intent?, success: SuccessCallback, fail: FailCallback) {

        if (data != null) {
            LocalDisk.accessGrantType = LocalDisk.GRANT_TYPE_TOKEN
        }

        val getProfile = connectAPIManager.getProfile()
            .map {
                AppLog.e("user profile: ${it.result}")
                //save profile to db
                appDatabaseManager.deleteUserProfile()
                appDatabaseManager.saveUserProfile(it.result)
                LocalDisk.userProfile = it.result
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                success.invoke()
            }, {
                AppLog.e("error: ${it.message.toString()}")
                fail.invoke()
            })
        compositeDisposable.add(getProfile)
    }

}