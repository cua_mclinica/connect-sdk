package com.mclinica.connect.platform.connectsdk.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.mclinica.connect.platform.connectsdk.R
import com.mclinica.connect.platform.connectsdk.ui.login.LoginActivity
import com.mclinica.connect.platform.connectsdk.ui.webview.UserGuideActivity
import com.mclinica.connect.platform.sdk.base.BaseFragment
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment() {

    private lateinit var mainViewModel: MainViewModel

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun initViewModel() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    private fun setupUI() {

        btnUserGuide.setOnClickListener {
            activity!!.startActivity(UserGuideActivity.getIntent(activity!!))
        }

        btnLogOut.setOnClickListener {
            mainViewModel.logout({
                startActivity(LoginActivity.getIntent(activity!!))
                activity!!.finish()
            }, {
                AppLog.e("Can't log out!")
            })
        }
    }

}