package com.mclinica.connect.platform.connectsdk.di

import android.content.Context
import com.mclinica.connect.platform.connectsdk.data.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideDataSource(context: Context): AppDatabase =
        AppDatabase.buildDatabase(context)

}