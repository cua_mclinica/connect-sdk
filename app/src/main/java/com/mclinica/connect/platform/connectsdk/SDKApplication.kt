package com.mclinica.connect.platform.connectsdk

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.mclinica.connect.platform.connectsdk.di.AppComponent
import com.mclinica.connect.platform.connectsdk.di.DaggerAppComponent
import com.mclinica.connect.platform.connectsdk.di.RoomModule
import com.mclinica.connect.platform.sdk.ConnectSDK
import com.mclinica.connect.platform.sdk.data.AuthAPIManager
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.data.LocalDisk.GRANT_TYPE_PASSWORD
import com.mclinica.connect.platform.sdk.data.LocalDisk.GRANT_TYPE_TOKEN
import com.mclinica.connect.platform.sdk.data.bus.BusEvent
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import com.mclinica.connect.platform.sdk.di.AppModule
import com.mclinica.connect.platform.sdk.di.AuthRemoteModule
import com.mclinica.connect.platform.sdk.di.ConnectRemoteModule
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SDKApplication : Application(), LifecycleObserver {

    companion object {

        lateinit var appComponent: AppComponent
        lateinit var dateSimulation: String //for debugging date simulations

        const val IS_PRODUCTION: Boolean = "prd" == BuildConfig.FLAVOR

        val versionName: String = if (IS_PRODUCTION) {
            BuildConfig.VERSION_NAME
        } else {
            "${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})"
        }

    }

    @Inject
    lateinit var rxEventBus: RxEventBus

    @Inject
    lateinit var authAPIManager: AuthAPIManager

    @Inject
    lateinit var sharedPreferences: RxSharedPreferences

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        ConnectSDK.initialize(
            this,
            BuildConfig.ENDPOINT,
            BuildConfig.PROGRAM_ID,
            BuildConfig.VERSION_NAME,
            !IS_PRODUCTION
        )

        initDagger()
        checkAndUpdateAccessToken()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .connectRemoteModule(ConnectRemoteModule())
            .authRemoteModule(AuthRemoteModule())
            .roomModule(RoomModule())
            .build()
        appComponent.inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun appBackgrounded() {
        AppLog.d("The app has been backgrounded")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun appForegrounded() {
        AppLog.d("The app has been foregrounded")
    }

    private fun checkAndUpdateAccessToken() {
        val authErrorCheck = rxEventBus.filteredObservable(BusEvent.AuthenticationError::class.java)
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe {
                //this will be triggered when an api call is unauthorized
                //maybe add refresh token api here
                AppLog.e("AuthenticationError: Refresh Token")
                when (LocalDisk.accessGrantType) {
                    GRANT_TYPE_TOKEN -> {
                        //refresh token using keycloak auth
                        val refreshToken = authAPIManager.refreshToken().subscribe({ authToken ->
                            LocalDisk.authToken = authToken
                        }, {
                            AppLog.e("error: ${it.message.toString()}")
                        })
                        compositeDisposable.add(refreshToken)
                    }
                    GRANT_TYPE_PASSWORD -> {
                        //refresh token with grant type password
                        val refreshToken = authAPIManager.getAuthToken(
                            LocalDisk.username!!,
                            LocalDisk.password!!
                        ).subscribe({ authToken ->
                            LocalDisk.authToken = authToken
                        }, {
                            AppLog.e("error: ${it.message.toString()}")
                        })
                        compositeDisposable.add(refreshToken)
                    }
                }
            }
        compositeDisposable.add(authErrorCheck)
    }

}