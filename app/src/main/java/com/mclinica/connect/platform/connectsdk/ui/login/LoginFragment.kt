package com.mclinica.connect.platform.connectsdk.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mclinica.connect.platform.connectsdk.R
import com.mclinica.connect.platform.connectsdk.SDKApplication
import com.mclinica.connect.platform.connectsdk.ui.main.MainActivity
import com.mclinica.connect.platform.connectsdk.ui.registration.RegistrationActivity
import com.mclinica.connect.platform.sdk.ConnectSDK
import com.mclinica.connect.platform.sdk.auth.Auth
import com.mclinica.connect.platform.sdk.base.BaseFragment
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {

    private lateinit var loginViewModel: LoginViewModel

    companion object {
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun initViewModel() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    private fun setupUI() {

        //listen to network connectivity real time
        ConnectSDK.networkConnectivity.observe(this, Observer { isOnline ->
            if (!isOnline) {
                showCrouton("You are now offline.")
            }
        })

        tvProgramId.text = "${ConnectSDK.PROGRAM_ID} ${SDKApplication.versionName}"

        btnRefresh.setOnClickListener {
            getAppVersion()
        }

        btnSignIn.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            loginViewModel.login(this) {
                progressBar.visibility = View.GONE
            }
        }

        btnManualSignIn.setOnClickListener {
            loginViewModel.manualLogin(inputUsername, inputPassword, {
                //update profile then go to main activity
                progressBar.visibility = View.VISIBLE
                loginViewModel.getProfile({
                    startActivity(MainActivity.getIntent(activity!!))
                    activity!!.finish()
                }, {
                    AppLog.e("Error in getting user profile")
                })
            }, {
                progressBar.visibility = View.GONE
            })
        }

        tvRegister.setOnClickListener {
            activity!!.startActivity(RegistrationActivity.getIntent(activity!!))
        }

        getAppVersion()
    }

    private fun getAppVersion() {
        progressBar.visibility = View.VISIBLE
        grpRefresh.visibility = View.GONE
        grpLogin.visibility = View.GONE

        loginViewModel.checkAppVersion({
            if (LocalDisk.isLoggedIn!!) {
                //go directly to main activity after checking config
                startActivity(MainActivity.getIntent(activity!!))
                activity!!.finish()
            } else {
                progressBar.visibility = View.GONE
                grpLogin.visibility = View.VISIBLE
            }
        }, {
            progressBar.visibility = View.GONE
            grpRefresh.visibility = View.VISIBLE
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        progressBar.visibility = View.GONE
        Auth.auth().retrieveToken(requestCode, data, {
            //get user profile first then go to main activity
            loginViewModel.getProfile(data, {
                startActivity(MainActivity.getIntent(activity!!))
                activity!!.finish()
            }, {
                AppLog.e("Error in getting user profile")
            })
        }, {
            AppLog.e("Error in authenticating with keycloak")
        })
    }

}