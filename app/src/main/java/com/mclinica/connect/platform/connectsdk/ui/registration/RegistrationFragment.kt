package com.mclinica.connect.platform.connectsdk.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.mclinica.connect.platform.connectsdk.R
import com.mclinica.connect.platform.sdk.base.BaseFragment

class RegistrationFragment : BaseFragment() {

    private lateinit var registrationViewModel: RegistrationViewModel

    companion object {
        fun newInstance(): RegistrationFragment {
            return RegistrationFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun initViewModel() {
        registrationViewModel = ViewModelProviders.of(this).get(RegistrationViewModel::class.java)
    }

    private fun setupUI() {

    }

}