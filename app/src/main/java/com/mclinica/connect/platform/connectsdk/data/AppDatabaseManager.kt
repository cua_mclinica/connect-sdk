package com.mclinica.connect.platform.connectsdk.data

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.mclinica.connect.platform.sdk.auth.KeyCloakConfiguration
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import com.mclinica.connect.platform.sdk.data.model.User
import com.mclinica.connect.platform.sdk.helper.callback.FailCallbackThrowable
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallbackType
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDatabaseManager @Inject constructor(
    private val sharedPreferences: RxSharedPreferences,
    private val rxEventBus: RxEventBus,
    private val appDatabase: AppDatabase
) {

    private val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    fun getConfig(
        success: SuccessCallbackType<KeyCloakConfiguration>,
        fail: FailCallbackThrowable
    ) {
        val disposable = appDatabase.configDao().getConfiguration()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                success.invoke(it)
            }, {
                AppLog.e(it.localizedMessage.toString())
                fail.invoke(it)
            })
        allCompositeDisposable.add(disposable)
    }

    fun deleteConfig() {
        appDatabase.configDao().deleteConfiguration()
    }

    fun saveConfig(keyCloakConfiguration: KeyCloakConfiguration) {
        appDatabase.configDao().insert(keyCloakConfiguration)
    }

    fun deleteUserProfile(){
        appDatabase.userProfileDao().deleteUserProfile()
    }

    fun saveUserProfile(userProfile: User.Profile){
        appDatabase.userProfileDao().insert(userProfile)
    }

}