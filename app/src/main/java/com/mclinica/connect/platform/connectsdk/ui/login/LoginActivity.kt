package com.mclinica.connect.platform.connectsdk.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.mclinica.connect.platform.connectsdk.R
import com.mclinica.connect.platform.sdk.base.BaseActivity

class LoginActivity : BaseActivity() {

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (savedInstanceState == null) {
            val fragment = LoginFragment.newInstance()
            supportFragmentManager.beginTransaction().replace(R.id.lin_container, fragment).commit()
        }
    }

}
