package com.mclinica.connect.platform.connectsdk.ui.registration

import androidx.lifecycle.ViewModel
import com.mclinica.connect.platform.connectsdk.SDKApplication

class RegistrationViewModel : ViewModel() {

    init {
        SDKApplication.appComponent.inject(this)
    }

}