package com.mclinica.connect.platform.connectsdk.di

import com.mclinica.connect.platform.connectsdk.SDKApplication
import com.mclinica.connect.platform.connectsdk.ui.login.LoginViewModel
import com.mclinica.connect.platform.connectsdk.ui.main.MainViewModel
import com.mclinica.connect.platform.connectsdk.ui.registration.RegistrationViewModel
import com.mclinica.connect.platform.sdk.di.AppModule
import com.mclinica.connect.platform.sdk.di.AuthRemoteModule
import com.mclinica.connect.platform.sdk.di.ConnectRemoteModule
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, ConnectRemoteModule::class, AuthRemoteModule::class, RoomModule::class])
@Singleton
interface AppComponent {

    fun inject(sdkApplication: SDKApplication)

    fun inject(loginViewModel: LoginViewModel)

    fun inject(mainViewModel: MainViewModel)

    fun inject(registrationViewModel: RegistrationViewModel)

}