package com.mclinica.connect.platform.connectsdk.ui.main

import androidx.lifecycle.ViewModel
import com.mclinica.connect.platform.connectsdk.SDKApplication
import com.mclinica.connect.platform.sdk.data.ConnectAPIManager
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.helper.callback.FailCallbackThrowable
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallback
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel : ViewModel() {

    @Inject
    lateinit var connectAPIManager: ConnectAPIManager

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        SDKApplication.appComponent.inject(this)
    }

    fun logout(success: SuccessCallback, fail: FailCallbackThrowable) {
        val logout = connectAPIManager.logout(LocalDisk.pushKey!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                LocalDisk.isLoggedIn = false
                success.invoke()
            }, {
                fail.invoke(it)
            })
        compositeDisposable.add(logout)
    }

}