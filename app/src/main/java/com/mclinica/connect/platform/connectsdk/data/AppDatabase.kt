package com.mclinica.connect.platform.connectsdk.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mclinica.connect.platform.sdk.auth.KeyCloakConfiguration
import com.mclinica.connect.platform.sdk.data.dao.ConfigDao
import com.mclinica.connect.platform.sdk.data.dao.UserProfileDao
import com.mclinica.connect.platform.sdk.data.model.User
import com.mclinica.connect.platform.sdk.helper.network.JavaConverter

@Database(
    entities = [KeyCloakConfiguration::class, User.Profile::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(JavaConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun configDao(): ConfigDao

    abstract fun userProfileDao(): UserProfileDao

    companion object {
        fun buildDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "AppDatabase.db"
            ).fallbackToDestructiveMigration().build()
    }

}