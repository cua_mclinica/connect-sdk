package com.mclinica.connect.platform.sdk.di

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mclinica.connect.platform.sdk.ConnectSDK
import com.mclinica.connect.platform.sdk.data.ConnectRemoteAPI
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import com.mclinica.connect.platform.sdk.helper.network.APINetworkInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class ConnectRemoteModule {

    companion object {
        private const val interceptorName = "ConnectInterceptor"
        private const val okHttpName = "ConnectOKHttp"
        private const val retrofitName = "ConnectRetrofit"
    }

    @Singleton
    @Provides
    @Named(interceptorName)
    fun provideOFInterceptor(
        sharedPreferences: RxSharedPreferences,
        rxEventBus: RxEventBus
    ): Interceptor {
        return APINetworkInterceptor(
            sharedPreferences,
            rxEventBus,
            true
        )
    }

    @Singleton
    @Provides
    @Named(okHttpName)
    fun provideOFOkHttp(@Named(interceptorName) interceptor: Interceptor): OkHttpClient {
        val logger = HttpLoggingInterceptor()

        if (ConnectSDK.ENABLE_LOGS) {
            logger.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logger.level = HttpLoggingInterceptor.Level.NONE
        }

        return OkHttpClient.Builder()
            .addInterceptor(logger)
            .addInterceptor(interceptor)
            .addNetworkInterceptor(StethoInterceptor())
            .build()
    }

    @Singleton
    @Provides
    @Named(retrofitName)
    fun provideRetrofit(@Named(okHttpName) okHttpClient: OkHttpClient): Retrofit {
        val gson: Gson = GsonBuilder().create()
        return Retrofit.Builder()
            .baseUrl(ConnectSDK.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideConnectAPIService(@Named(retrofitName) retrofit: Retrofit): ConnectRemoteAPI =
        retrofit.create(ConnectRemoteAPI::class.java)
}