package com.mclinica.connect.platform.sdk.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideRXSharedPrefs(context: Context): RxSharedPreferences {
        val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return RxSharedPreferences.create(preferences)
    }

    @Provides
    @Singleton
    fun provideEventBus(): RxEventBus {
        return RxEventBus()
    }

}