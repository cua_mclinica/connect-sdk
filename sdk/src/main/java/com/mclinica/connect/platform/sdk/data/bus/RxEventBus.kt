package com.mclinica.connect.platform.sdk.data.bus

import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RxEventBus @Inject
constructor() {

    private val mBusSubject: PublishProcessor<Any> = PublishProcessor.create()

    /**
     * Posts an object (usually an Event) to the bus
     */
    fun post(event: Any) {
        mBusSubject.onNext(event)
    }

    /**
     * Observable that will emmit everything posted to the event bus.
     */
    fun observable(): Flowable<Any> {
        return mBusSubject
    }

    /**
     * Observable that only emits events of a specific class.
     * Use this if you only want to subscribe to one type of events.
     */
    fun <T> filteredObservable(eventClass: Class<T>): Flowable<T> {
        return mBusSubject.ofType(eventClass)
    }

}