package com.mclinica.connect.platform.sdk.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mclinica.connect.platform.sdk.auth.KeyCloakConfiguration
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface ConfigDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(config: KeyCloakConfiguration)

    @Query("SELECT * FROM config")
    fun getConfiguration(): Flowable<KeyCloakConfiguration>

    @Query("SELECT * FROM config")
    fun getConfigurationMaybe(): Maybe<KeyCloakConfiguration>

    @Query("DELETE FROM config")
    fun deleteConfiguration()

}