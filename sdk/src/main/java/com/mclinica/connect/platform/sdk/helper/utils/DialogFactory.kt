package com.mclinica.connect.platform.sdk.helper.utils

import android.app.Dialog
import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.mclinica.connect.platform.sdk.helper.callback.FailCallback
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallback
import com.mclinica.connect.platform.sdk.helper.utils.Utility.openPlayStore

object DialogFactory {

    fun createOkDialog(
        context: Context?,
        message: String?,
        action: String?,
        successCallback: SuccessCallback
    ): Dialog? {
        val alertDialog =
            AlertDialog.Builder(context!!)
                .setMessage(message)
                .setPositiveButton(action)
                { _, _ ->
                    successCallback.invoke()
                }
        return alertDialog.create()
    }

    fun createOkDialog(
        context: Context?,
        @StringRes messageResource: Int,
        @StringRes actionResource: Int,
        successCallback: SuccessCallback
    ): Dialog? {
        return createOkDialog(
            context,
            context?.getString(messageResource),
            context?.getString(actionResource),
            successCallback
        )
    }

    fun createOkDialog(
        context: Context?,
        title: String?,
        message: String?,
        action: String?,
        successCallback: SuccessCallback
    ): Dialog? {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(action)
            { _, _ ->
                successCallback.invoke()
            }
        return alertDialog.create()
    }

    fun createOkDialog(
        context: Context?,
        @StringRes titleResource: Int,
        @StringRes messageResource: Int,
        @StringRes actionResource: Int,
        successCallback: SuccessCallback
    ): Dialog? {
        return createOkDialog(
            context,
            context?.getString(titleResource),
            context?.getString(messageResource),
            context?.getString(actionResource),
            successCallback
        )
    }

    fun createOkCancelDialog(
        context: Context?,
        message: String?,
        actionPositive: String?,
        actionNegative: String?,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ): Dialog? {
        val alertDialog =
            AlertDialog.Builder(context!!)
                .setMessage(message)
                .setPositiveButton(actionPositive)
                { _, _ ->
                    successCallback.invoke()
                }
                .setNegativeButton(actionNegative)
                { _, _ ->
                    failCallback.invoke()
                }
        return alertDialog.create()
    }

    fun createOkCancelDialog(
        context: Context?,
        @StringRes messageResource: Int,
        @StringRes actionPositiveResource: Int,
        @StringRes actionNegativeResource: Int,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ): Dialog? {
        return createOkCancelDialog(
            context,
            context?.getString(messageResource),
            context?.getString(actionPositiveResource),
            context?.getString(actionNegativeResource),
            successCallback,
            failCallback
        )
    }

    fun createOkCancelDialog(
        context: Context?,
        title: String?,
        message: String?,
        actionPositive: String?,
        actionNegative: String?,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ): Dialog? {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(actionPositive)
            { _, _ ->
                successCallback.invoke()
            }
            .setNegativeButton(actionNegative)
            { _, _ ->
                failCallback.invoke()
            }
        return alertDialog.create()
    }

    fun createOkCancelDialog(
        context: Context?,
        @StringRes titleResource: Int,
        @StringRes messageResource: Int,
        @StringRes actionPositiveResource: Int,
        @StringRes actionNegativeResource: Int,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ): Dialog? {
        return createOkCancelDialog(
            context,
            context?.getString(titleResource),
            context?.getString(messageResource),
            context?.getString(actionPositiveResource),
            context?.getString(actionNegativeResource),
            successCallback,
            failCallback
        )
    }

    fun createUpgradeAppDialog(
        context: Context?,
        title: String?,
        message: String?,
        action: String?,
        packageName: String?
    ): Dialog? {
        val upgradeDialog =
            AlertDialog.Builder(context!!)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(action)
                { dialog, which ->
                    openPlayStore(context, packageName)
                }
        return upgradeDialog.create()
    }

    fun createUpgradeAppDialog(
        context: Context?,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes action: Int,
        packageName: String?
    ): Dialog? {
        val upgradeDialog =
            AlertDialog.Builder(context!!)
                .setTitle(context.getString(title))
                .setMessage(context.getString(message))
                .setCancelable(false)
                .setPositiveButton(context.getString(action))
                { dialog, which ->
                    openPlayStore(context, packageName)
                }
        return upgradeDialog.create()
    }


}