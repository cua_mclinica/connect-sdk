package com.mclinica.connect.platform.sdk.base

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.mclinica.connect.platform.sdk.helper.callback.FailCallback
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallback
import com.mclinica.connect.platform.sdk.helper.utils.CroutonsHelper
import com.mclinica.connect.platform.sdk.helper.utils.DialogFactory
import com.mclinica.connect.platform.sdk.ui.snackbar.ConnectSnackbar

abstract class BaseActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_RECORD_AUDIO = 4001
        const val REQUEST_RECORD_VIDEO = 4002
    }

    fun setStatusBarColor(color: Int) {
        val view = findViewById<View>(android.R.id.content)
        val window = window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = color
            var flags = view.systemUiVisibility
            flags = flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            view.systemUiVisibility = flags
        }
    }

    fun dismissKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.currentFocus?.windowToken, 0)
    }

    fun showCrouton(msg: String) {
        CroutonsHelper.show(this, msg, 0)
    }

    fun showCrouton(msg: String, @IdRes view: Int) {
        CroutonsHelper.show(this, msg, 0, view)
    }

    fun showSuccess(message: String, view: View) {
        ConnectSnackbar.showSuccess(this, view, message)
    }

    fun showError(message: String, view: View) {
        ConnectSnackbar.showError(this, view, message)
    }

    fun showWarning(message: String, view: View) {
        ConnectSnackbar.showWarning(this, view, message)
    }

    fun showInfo(message: String, view: View) {
        ConnectSnackbar.showInfo(this, view, message)
    }

    fun openWebPage(url: String?) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

    fun showOkDialog(
        context: Context?,
        message: String?,
        action: String?,
        successCallback: SuccessCallback
    ) {
        DialogFactory.createOkDialog(context, message, action, successCallback)?.show()
    }

    fun showOkDialog(
        context: Context?,
        @StringRes message: Int,
        @StringRes action: Int,
        successCallback: SuccessCallback
    ) {
        DialogFactory.createOkDialog(context, message, action, successCallback)?.show()
    }

    fun showOkDialog(
        context: Context?,
        title: String?,
        message: String?,
        action: String?,
        successCallback: SuccessCallback
    ) {
        DialogFactory.createOkDialog(context, title, message, action, successCallback)?.show()
    }

    fun showOkDialog(
        context: Context?,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes action: Int,
        successCallback: SuccessCallback
    ) {
        DialogFactory.createOkDialog(context, title, message, action, successCallback)?.show()
    }

    fun showOkCancelDialog(
        context: Context?,
        message: String?,
        actionPositive: String?,
        actionNegative: String?,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        DialogFactory.createOkCancelDialog(
            context,
            message,
            actionPositive,
            actionNegative,
            successCallback,
            failCallback
        )?.show()
    }

    fun showOkCancelDialog(
        context: Context?,
        @StringRes message: Int,
        @StringRes actionPositiveResource: Int,
        @StringRes actionNegativeResource: Int,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        DialogFactory.createOkCancelDialog(
            context,
            message,
            actionPositiveResource,
            actionNegativeResource,
            successCallback,
            failCallback
        )?.show()
    }

    fun showOkCancelDialog(
        context: Context?,
        title: String?,
        message: String?,
        actionPositive: String?,
        actionNegative: String?,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        DialogFactory.createOkCancelDialog(
            context,
            title,
            message,
            actionPositive,
            actionNegative,
            successCallback,
            failCallback
        )?.show()
    }

    fun showOkCancelDialog(
        context: Context?,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes actionPositiveResource: Int,
        @StringRes actionNegativeResource: Int,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        DialogFactory.createOkCancelDialog(
            context,
            title,
            message,
            actionPositiveResource,
            actionNegativeResource,
            successCallback,
            failCallback
        )?.show()
    }

    fun showUpgradeAppDialog(
        context: Context?,
        title: String?,
        message: String?,
        action: String?,
        packageName: String?
    ) {
        DialogFactory.createUpgradeAppDialog(context, title, message, action, packageName)?.show()
    }

    fun showUpgradeAppDialog(
        context: Context?,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes action: Int,
        packageName: String?
    ) {
        DialogFactory.createUpgradeAppDialog(context, title, message, action, packageName)?.show()
    }

    fun checkPermission(permission: String): Boolean {
        val permission = ActivityCompat.checkSelfPermission(this, permission)
        if (permission == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    //record audio
    fun verifyRecordAudioPermission(): Boolean {
        val permission =
            PermissionChecker.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.RECORD_AUDIO),
                REQUEST_RECORD_AUDIO
            )
        }
        return permission == PackageManager.PERMISSION_GRANTED
    }

    //take pictures and record video
    fun verifyTakePictureAndRecordVideoPermission(): Boolean {
        val permission = PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_RECORD_VIDEO
            )
        }
        return permission == PackageManager.PERMISSION_GRANTED
    }

}