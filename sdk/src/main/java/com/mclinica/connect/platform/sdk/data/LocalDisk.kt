package com.mclinica.connect.platform.sdk.data

import android.content.Context
import com.mclinica.connect.platform.sdk.auth.AuthToken
import com.mclinica.connect.platform.sdk.auth.ProgramConfiguration
import com.mclinica.connect.platform.sdk.data.model.User

/**
 * Class for adding/modifying cache values.
 */
object LocalDisk {

    const val GRANT_TYPE_PASSWORD = "password"
    const val GRANT_TYPE_TOKEN = "refresh_token"

    private const val PREF_CONFIGURATION = "connect_configuration"
    private const val PREF_USER_PROFILE  = "user_profile"
    private const val PREF_AUTH_TOKEN = "user_access_token"
    private const val IS_LOGGED_IN = "user_logged_in"
    private const val GRANT_TYPE = "access_grant_type"
    private const val USER_NAME = "access_username"
    private const val PASSWORD = "access_password"
    private const val FCM_PUSH_KEY = "firebase_push_key"
    private const val CONFIG_HASH = "config_hash"

    private var localDiskCacheBox: LocalDiskCacheBox? = null

    @Synchronized
    fun init(context: Context): LocalDiskCacheBox {
        if (localDiskCacheBox == null) localDiskCacheBox =
            LocalDiskCacheBox(context)
        return localDiskCacheBox!!
    }

    var appConfiguration: ProgramConfiguration?
        get() = localDiskCacheBox!![PREF_CONFIGURATION]
        set(value) {
            localDiskCacheBox!![PREF_CONFIGURATION] = value
        }

    var authToken: AuthToken?
        get() = localDiskCacheBox!![PREF_AUTH_TOKEN]
        set(value) {
            localDiskCacheBox!![PREF_AUTH_TOKEN] = value
        }

    var userProfile: User.Profile?
        get() = localDiskCacheBox!![PREF_USER_PROFILE]
        set(value) {
            localDiskCacheBox!![PREF_USER_PROFILE] = value
        }

    var accessGrantType: String?
        get() = localDiskCacheBox!![GRANT_TYPE] ?: ""
        set(value) {
            localDiskCacheBox!![GRANT_TYPE] = value
        }

    var username: String?
        get() = localDiskCacheBox!![USER_NAME] ?: ""
        set(value) {
            localDiskCacheBox!![USER_NAME] = value
        }

    var password: String?
        get() = localDiskCacheBox!![PASSWORD] ?: ""
        set(value) {
            localDiskCacheBox!![PASSWORD] = value
        }

    var isLoggedIn: Boolean?
        get() = localDiskCacheBox!![IS_LOGGED_IN] ?: false
        set(value) {
            localDiskCacheBox!![IS_LOGGED_IN] = value
        }

    var pushKey: String?
        get() = localDiskCacheBox!![FCM_PUSH_KEY] ?: ""
        set(value) {
            localDiskCacheBox!![FCM_PUSH_KEY] = value
        }

    var configHash: String?
        get() = localDiskCacheBox!![CONFIG_HASH] ?: ""
        set(value) {
            localDiskCacheBox!![CONFIG_HASH] = value
        }

}