package com.mclinica.connect.platform.sdk.data.model

import com.google.gson.annotations.SerializedName

data class TransactionsConfiguration(
    @SerializedName("purchase_transaction") val purchaseTransaction: PurchaseTransaction,
    @SerializedName("redeem_transaction") val redeemTransaction: RedeemTransaction,
    @SerializedName("void_transaction") val voidTransaction: VoidTransaction
) {

    data class PurchaseTransaction(
        @SerializedName("is_enabled") val isEnabled: Boolean,
        @SerializedName("transaction_field_configuration") val purchaseTransactionFieldConfiguration: List<UsersConfiguration.FieldConfiguration>
    )

    data class RedeemTransaction(
        @SerializedName("is_enabled") val isEnabled: Boolean,
        @SerializedName("transaction_field_configuration") val redeemTransactionFieldConfiguration: List<UsersConfiguration.FieldConfiguration>
    )

    data class VoidTransaction(
        @SerializedName("is_enabled") val isEnabled: Boolean
    )

}