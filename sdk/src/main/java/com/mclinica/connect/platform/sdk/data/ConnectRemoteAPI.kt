package com.mclinica.connect.platform.sdk.data

import com.mclinica.connect.platform.sdk.auth.ProgramConfiguration
import com.mclinica.connect.platform.sdk.data.model.APIResponse
import com.mclinica.connect.platform.sdk.data.model.AppVersion
import com.mclinica.connect.platform.sdk.data.model.User
import io.reactivex.Observable
import retrofit2.http.*

interface ConnectRemoteAPI {

    @GET("programs/{program_id}/version")
    fun getVersion(@Path("program_id") programId: String): Observable<AppVersion>

    @GET("programs/{program_id}/config")
    fun getConfig(@Path("program_id") programId: String): Observable<ProgramConfiguration>

    @GET("users/{program_id}/profile")
    fun getProfile(@Path("program_id") programId: String): Observable<User>

    @FormUrlEncoded
    @POST("users/{program_id}/auth/logout")
    fun logout(
        @Path("program_id") programId: String,
        @Field("push_key") pushKey: String
    ): Observable<APIResponse>

}