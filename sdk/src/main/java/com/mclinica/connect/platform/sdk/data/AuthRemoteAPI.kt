package com.mclinica.connect.platform.sdk.data

import com.mclinica.connect.platform.sdk.auth.AuthToken
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Path

interface AuthRemoteAPI {

    @FormUrlEncoded
    @POST("auth/realms/{program_id}/protocol/openid-connect/token")
    fun getAuthToken(
        @Path("program_id") programId: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String = "password",
        @Field("client_id") clientId: String = "public"
    ): Observable<AuthToken>

    @FormUrlEncoded
    @POST("auth/realms/{program_id}/protocol/openid-connect/token")
    fun refreshToken(
        @Path("program_id") programId: String,
        @Field("refresh_token") refreshToken: String,
        @Field("grant_type") grantType: String = "refresh_token",
        @Field("client_id") clientId: String = "public"
    ): Observable<AuthToken>

}