package com.mclinica.connect.platform.sdk.ui.snackbar

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.snackbar.ContentViewCallback
import com.mclinica.connect.platform.sdk.R

class ConnectSnackbarView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), ContentViewCallback {

    private var icon: ImageView
    private var bg: View

    companion object {
        const val TYPE_SUCCESS = 1
        const val TYPE_ERROR = 2
        const val TYPE_INFO = 3
        const val TYPE_WARNING = 4
    }

    init {
        View.inflate(context, R.layout.connect_snackbar_view, this)
        clipToPadding = false
        this.icon = findViewById(R.id.icon)
        this.bg = findViewById(R.id.background)
    }

    fun setType(context: Context, type: Int) {
        icon.setImageDrawable(getIcon(context, type))
        bg.setBackgroundDrawable(getBackground(context, type))
    }

    private fun getIcon(context: Context, type: Int): Drawable {
        return when (type) {
            TYPE_SUCCESS -> context.resources.getDrawable(R.drawable.ic_snackbar_success)
            TYPE_ERROR -> context.resources.getDrawable(R.drawable.ic_snackbar_error)
            TYPE_INFO -> context.resources.getDrawable(R.drawable.ic_snackbar_info)
            TYPE_WARNING -> context.resources.getDrawable(R.drawable.ic_snackbar_warning)
            else -> context.resources.getDrawable(R.drawable.ic_snackbar_success)
        }
    }

    private fun getBackground(context: Context, type: Int): Drawable {
        return when (type) {
            TYPE_SUCCESS -> context.resources.getDrawable(R.drawable.bg_snackbar_success)
            TYPE_ERROR -> context.resources.getDrawable(R.drawable.bg_snackbar_error)
            TYPE_INFO -> context.resources.getDrawable(R.drawable.bg_snackbar_info)
            TYPE_WARNING -> context.resources.getDrawable(R.drawable.bg_snackbar_warning)
            else -> context.resources.getDrawable(R.drawable.bg_snackbar_success)
        }
    }

    override fun animateContentIn(delay: Int, duration: Int) {
        // we can add lottie here soon
        val scaleX = ObjectAnimator.ofFloat(icon, View.SCALE_X, 0f, 1f)
        val scaleY = ObjectAnimator.ofFloat(icon, View.SCALE_Y, 0f, 1f)
        val animatorSet = AnimatorSet().apply {
            interpolator = OvershootInterpolator()
            setDuration(500)
            playTogether(scaleX, scaleY)
        }
        animatorSet.start()
    }

    override fun animateContentOut(delay: Int, duration: Int) {
    }

}