package com.mclinica.connect.platform.sdk.data

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.mclinica.connect.platform.sdk.ConnectSDK
import com.mclinica.connect.platform.sdk.auth.AuthToken
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthAPIManager @Inject constructor(
    private val authRemoteAPI: AuthRemoteAPI,
    private val sharedPreferences: RxSharedPreferences,
    private val rxEventBus: RxEventBus
) {

    fun getAuthToken(username: String, password: String): Observable<AuthToken> {
        return authRemoteAPI.getAuthToken(ConnectSDK.PROGRAM_ID, username, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun refreshToken(): Observable<AuthToken> {
        return authRemoteAPI.refreshToken(ConnectSDK.PROGRAM_ID, LocalDisk.authToken?.refreshToken!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}