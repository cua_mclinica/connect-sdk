package com.mclinica.connect.platform.sdk.data

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.mclinica.connect.platform.sdk.ConnectSDK
import com.mclinica.connect.platform.sdk.auth.ProgramConfiguration
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import com.mclinica.connect.platform.sdk.data.model.APIResponse
import com.mclinica.connect.platform.sdk.data.model.AppVersion
import com.mclinica.connect.platform.sdk.data.model.User
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectAPIManager @Inject constructor(
    private val remoteAPI: ConnectRemoteAPI,
    private val sharedPreferences: RxSharedPreferences,
    private val rxEventBus: RxEventBus
) {

    fun getVersion(): Observable<AppVersion> {
        return remoteAPI.getVersion(ConnectSDK.PROGRAM_ID)
    }

    fun getConfigFromAPI(): Observable<ProgramConfiguration> {
        return remoteAPI.getConfig(ConnectSDK.PROGRAM_ID)
    }

    fun getProfile(): Observable<User> {
        return remoteAPI.getProfile(ConnectSDK.PROGRAM_ID)
    }

    fun logout(): Observable<APIResponse> {
        return logout("")
    }

    fun logout(pushKey: String): Observable<APIResponse> {
        return remoteAPI.logout(ConnectSDK.PROGRAM_ID, pushKey)
    }

}