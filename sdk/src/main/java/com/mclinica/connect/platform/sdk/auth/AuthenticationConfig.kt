package com.mclinica.connect.platform.sdk.auth

data class AuthenticationConfig(
    val clientId: String,
    val issuer: String,
    val redirectUri: String?,
    val tokenEndpointUri: String,
    val responseType: String
)