package com.mclinica.connect.platform.sdk.helper.utils

import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.Type

class RxErrorHandlingCallAdapterFactory : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        TODO("Let's do this for improvents - Mark Cua")
        // Let's add custom error handling for retrofit
    }

}