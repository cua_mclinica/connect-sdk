package com.mclinica.connect.platform.sdk.auth

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import net.openid.appauth.AuthorizationException

internal interface AuthenticationService {

    /**
     * Opens a web view for the user to login. For fragments.
     *
     * @param failed (AuthorizationException?) -> Unit, fail callback when auth or config fails
     * @param start (() -> Unit)?, callback before opening the web view
     * @param finished (() -> Unit)?, callback after the authentication is finished
     */
    fun connect(
        fragment: Fragment,
        failed: (AuthorizationException?) -> Unit,
        start: (() -> Unit)? = null,
        finished: (() -> Unit)? = null
    )

    /**
     * Opens a web view for the user to login.
     *
     * @param activity Activity, current activity
     * @param failed (AuthorizationException?) -> Unit, fail callback when auth or config fails
     * @param start (() -> Unit)?, callback before opening the web view
     * @param finished (() -> Unit)?, callback after the authentication is finished
     */
    fun connect(
        activity: Activity,
        failed: (AuthorizationException?) -> Unit,
        start: (() -> Unit)? = null,
        finished: (() -> Unit)? = null
    )

    /**
     * Opens a web view for the user to login with pending intent, which will be used if you handle
     * the retrieval of config to other activity.
     *
     * @param context Context, current context
     * @param pendingIntent PendingIntent, intent which will you intend to return the config
     * @param failed (AuthorizationException?) -> Unit, fail callback when auth or config fails
     * @param start (() -> Unit)?, callback before opening the web view
     * @param finished (() -> Unit)?, callback after the authentication is finished
     */
    fun connect(
        context: Context,
        pendingIntent: PendingIntent,
        failed: (AuthorizationException?) -> Unit,
        start: (() -> Unit)? = null,
        finished: (() -> Unit)? = null
    )

    /**
     * Retrieves the tokens requested from the config.
     *
     * @param intent Intent?, intent data which contains config response from the auth
     * @param success (() -> Unit)?, success callback
     * @param failed ((AuthorizationException?) -> Unit)?, fail callback
     */
    fun retrieveToken(
        intent: Intent?,
        success: (() -> Unit)? = null,
        failed: ((AuthorizationException?) -> Unit)? = null
    )

    /**
     * Retrieves the tokens requested from the config.
     *
     * @param requestCode Int, static request code
     * @param intent Intent?, intent data which contains config response from the auth
     * @param success (() -> Unit)?, success callback
     * @param failed ((AuthorizationException?) -> Unit)?, fail callback
     */
    fun retrieveToken(
        requestCode: Int = Auth.RC_AUTH,
        intent: Intent?,
        success: () -> Unit,
        failed: (AuthorizationException?) -> Unit
    )

}