package com.mclinica.connect.platform.sdk.data.model

import com.google.gson.annotations.SerializedName

data class CustomersConfiguration(
    @SerializedName("is_enabled") val isEnabled: Boolean,
    @SerializedName("customer_field_configuration") val customerFieldConfiguration: List<UsersConfiguration.FieldConfiguration>
)