package com.mclinica.connect.platform.sdk.helper.callback

typealias SuccessCallback = () -> Unit
typealias SuccessCallbackType<T> = (T) -> Unit
typealias FailCallback = () -> Unit
typealias FailCallbackThrowable = (Throwable) -> Unit