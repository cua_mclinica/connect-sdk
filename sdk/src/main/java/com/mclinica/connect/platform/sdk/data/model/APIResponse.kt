package com.mclinica.connect.platform.sdk.data.model

data class APIResponse(
    val status: Int,
    val message: String
)