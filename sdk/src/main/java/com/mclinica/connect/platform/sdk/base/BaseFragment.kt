package com.mclinica.connect.platform.sdk.base

import android.content.Context
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.mclinica.connect.platform.sdk.helper.callback.FailCallback
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallback

abstract class BaseFragment : Fragment() {

    fun dismissKeyboard() {
        (activity as BaseActivity).dismissKeyboard()
    }

    fun openWebPage(url: String?) {
        (activity as BaseActivity).openWebPage(url)
    }

    fun showCrouton(msg: String) {
        (activity as BaseActivity).showCrouton(msg)
    }

    fun showCrouton(msg: String, @IdRes view: Int) {
        (activity as BaseActivity).showCrouton(msg, view)
    }

    fun showSuccess(message: String, view: View) {
        (activity as BaseActivity).showSuccess(message, view)
    }

    fun showError(message: String, view: View) {
        (activity as BaseActivity).showError(message, view)
    }

    fun showWarning(message: String, view: View) {
        (activity as BaseActivity).showWarning(message, view)
    }

    fun showInfo(message: String, view: View) {
        (activity as BaseActivity).showInfo(message, view)
    }

    fun showOkDialog(message: String?, action: String?, successCallback: SuccessCallback) {
        (activity as BaseActivity).showOkDialog(activity, message, action, successCallback)
    }

    fun showOkDialog(
        @StringRes message: Int,
        @StringRes action: Int,
        successCallback: SuccessCallback
    ) {
        (activity as BaseActivity).showOkDialog(activity, message, action, successCallback)
    }

    fun showOkDialog(
        title: String?,
        message: String?,
        action: String?,
        successCallback: SuccessCallback
    ) {
        (activity as BaseActivity).showOkDialog(activity, title, message, action, successCallback)
    }

    fun showOkDialog(
        context: Context?,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes action: Int,
        successCallback: SuccessCallback
    ) {
        (activity as BaseActivity).showOkDialog(activity, title, message, action, successCallback)
    }

    fun showOkCancelDialog(
        message: String?,
        actionPositive: String?,
        actionNegative: String?,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        (activity as BaseActivity).showOkCancelDialog(
            activity,
            message,
            actionPositive,
            actionNegative,
            successCallback,
            failCallback
        )
    }

    fun showOkCancelDialog(
        @StringRes message: Int,
        @StringRes actionPositiveResource: Int,
        @StringRes actionNegativeResource: Int,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        (activity as BaseActivity).showOkCancelDialog(
            activity,
            message,
            actionPositiveResource,
            actionNegativeResource,
            successCallback,
            failCallback
        )
    }

    fun showOkCancelDialog(
        title: String?,
        message: String?,
        actionPositive: String?,
        actionNegative: String?,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        (activity as BaseActivity).showOkCancelDialog(
            activity,
            title,
            message,
            actionPositive,
            actionNegative,
            successCallback,
            failCallback
        )
    }

    fun showOkCancelDialog(
        context: Context?,
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes actionPositiveResource: Int,
        @StringRes actionNegativeResource: Int,
        successCallback: SuccessCallback,
        failCallback: FailCallback
    ) {
        (activity as BaseActivity).showOkCancelDialog(
            activity,
            title,
            message,
            actionPositiveResource,
            actionNegativeResource,
            successCallback,
            failCallback
        )
    }

    fun showUpgradeAppDialog(
        title: String?,
        message: String?,
        action: String?,
        packageName: String?
    ) {
        (activity as BaseActivity).showUpgradeAppDialog(
            activity,
            title,
            message,
            action,
            packageName
        )
    }

    fun showUpgradeAppDialog(
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes action: Int,
        packageName: String?
    ) {
        (activity as BaseActivity).showUpgradeAppDialog(
            activity,
            title,
            message,
            action,
            packageName
        )
    }

}