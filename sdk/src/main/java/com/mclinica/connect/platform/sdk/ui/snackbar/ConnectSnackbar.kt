package com.mclinica.connect.platform.sdk.ui.snackbar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.mclinica.connect.platform.sdk.R
import kotlinx.android.synthetic.main.connect_snackbar_view.view.*

class ConnectSnackbar(
    parent: ViewGroup,
    content: ConnectSnackbarView
) : BaseTransientBottomBar<ConnectSnackbar>(parent, content, content) {

    init {
        getView().setBackgroundColor(
            ContextCompat.getColor(
                view.context,
                android.R.color.transparent
            )
        )
        getView().setPadding(0, 0, 0, 0)
    }

    companion object {

        fun showSuccess(context: Context, view: View, message: String) {
            make(context, view, message, ConnectSnackbarView.TYPE_SUCCESS).show()
        }

        fun showError(context: Context, view: View, message: String) {
            make(context, view, message, ConnectSnackbarView.TYPE_ERROR).show()
        }

        fun showWarning(context: Context, view: View, message: String) {
            make(context, view, message, ConnectSnackbarView.TYPE_WARNING).show()
        }

        fun showInfo(context: Context, view: View, message: String) {
            make(context, view, message, ConnectSnackbarView.TYPE_INFO).show()
        }

        private fun make(context: Context, view: View, message: String, type: Int): ConnectSnackbar {

            val parent = view.findSuitableParent() ?: throw IllegalArgumentException(
                "No suitable parent found from the given view. Please provide a valid view."
            )
            val customView = LayoutInflater.from(view.context).inflate(
                R.layout.connect_snackbar, parent, false
            ) as ConnectSnackbarView

            customView.setType(context, type)
            val textView = customView.message
            textView.text = message

            return ConnectSnackbar(
                parent,
                customView
            )
        }

    }

}