package com.mclinica.connect.platform.sdk.auth

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.mclinica.connect.platform.sdk.ConnectSDK.REDIRECT_URL
import com.mclinica.connect.platform.sdk.helper.network.JavaConverter

@Entity(tableName = "config")
@TypeConverters(JavaConverter::class)
data class KeyCloakConfiguration(
    @SerializedName("realm") val realm: String,
    @SerializedName("resource") val resource: String,
    @SerializedName("client_id") val clientId: String,
    @SerializedName("bearer-only") val isBearerOnly: Boolean,
    @SerializedName("ssl-required") val sslRequired: String,
    @SerializedName("auth-server-url") val authUrl: String,
    @SerializedName("confidential-port") val port: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    fun authConfig(): AuthenticationConfig {
        val issuer = "$authUrl/realms/$realm"
        return AuthenticationConfig(
            clientId,
            issuer,
            REDIRECT_URL,
            "$issuer/protocol/openid-connect/token",
            "code"
        )
    }
}