package com.mclinica.connect.platform.sdk.data.model

import com.google.gson.annotations.SerializedName

data class RegisterResponse(
    @SerializedName("id") val id: String,
    @SerializedName("email") val email: String,
    @SerializedName("full_name") val fullname: String,
    @SerializedName("program_id") val programId: String,
    @SerializedName("user_type") val userType: String,
    @SerializedName("mobile_number") val mobileNumber: String,
    @SerializedName("status") val status: String,
    @SerializedName("mobile_verified_at") val mobileVerifiedAt: String?,
    @SerializedName("email_verified_at") val emailVerifiedAt: String?,
    @SerializedName("metadata") var metaData: User.Metadata?,
    @SerializedName("push_key") var pushKey: List<String>?,
    //
    @SerializedName("access_token") val accessToken: String?,
    @SerializedName("refresh_token") val refreshToken: String?,
    @SerializedName("expires_In") val expiresIn: Long?,
    @SerializedName("refresh_expires_in") val refreshExpiresIn: Long?,
    @SerializedName("token_type") val tokenType: String?,
    @SerializedName("scope") val scope: String?,

    //For MyCare POC
    @SerializedName("first_name") val firstName: String?,
    @SerializedName("last_name") val lastName: String?
)