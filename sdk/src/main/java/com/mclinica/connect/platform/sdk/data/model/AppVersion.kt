package com.mclinica.connect.platform.sdk.data.model

import com.google.gson.annotations.SerializedName

data class AppVersion(
    val status: Int,
    val message: String,
    @SerializedName("result") val result: Version
) {
    data class Version(
        @SerializedName("config_hash") val hash: String,
        @SerializedName("android") val android: Platform?,
        @SerializedName("ios") val ios: Platform?
    )

    data class Platform(
        @SerializedName("version") val version: String,
        @SerializedName("type") val type: String
    )
}