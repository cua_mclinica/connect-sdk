package com.mclinica.connect.platform.sdk.data.model

import androidx.room.*
import com.google.gson.annotations.SerializedName
import com.mclinica.connect.platform.sdk.helper.network.JavaConverter

data class User(
    val status: Int,
    val message: String,
    @SerializedName("result") val result: Profile
) {

    @Entity(tableName = "user_profile_data")
    @TypeConverters(JavaConverter::class)
    data class Profile(
        @ColumnInfo(name = "user_profile_id")
        @SerializedName("id") val user_profile_id: String?,
        @SerializedName("email") val email: String?,
        @SerializedName("first_name") val firstName: String?,
        @SerializedName("last_name") val lastName: String?,
        @SerializedName("full_name") val fullname: String?,
        @SerializedName("program_id") val programId: String?,
        @SerializedName("user_type") val userType: String?,
        @SerializedName("mobile_number") val mobileNumber: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("mobile_verified_at") val mobileVerifiedAt: String?,
        @SerializedName("email_verified_at") val emailVerifiedAt: String?,
        @Embedded
        @SerializedName("metadata") var metaData: Metadata?
    ) {
        @PrimaryKey(autoGenerate = true)
        var userId: Int = 0
    }

    data class Metadata(
        @SerializedName("nric") val nric: String?,
        @SerializedName("hospital") val hospital: String?
    )

}