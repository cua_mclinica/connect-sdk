package com.mclinica.connect.platform.sdk.helper.utils

import org.joda.time.format.ISODateTimeFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object DateTimeUtility {

    const val MonthDayYear = "MM/dd/yyyy"
    const val GMT_FORMAT = "yyyy-MM-dd'T'00:00:00'Z"
    const val ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS zzz"
    const val SERVER_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.000'Z'"

    // Get current device time
    private val currentTime: Long
        get() = Date().time

    //flexible date formatter - either choose from const or pass your own
    fun formatDate(transactionDate: String, dateFormat: String): String {
        val timeStamp = utcTimeConverter(transactionDate)
        val date = if (timeStamp == 0L) {
            Date(currentTime)
        } else {
            Date(timeStamp)
        }
        return SimpleDateFormat(dateFormat).format(date)
    }

    // Convert a string date to UTC format otherwise if an error occurred while converting it will return current device time
    private fun utcTimeConverter(date: String): Long {
        return try {
            ISODateTimeFormat.dateTimeParser().parseDateTime(date).millis
        } catch (e: Exception) {
            Date().time
        }
    }

    fun dateDuration(date: String): String {
        val format = SimpleDateFormat(SERVER_TIME_FORMAT)
        format.timeZone = TimeZone.getTimeZone("UTC")
        val past = format.parse(date)
        format.timeZone = TimeZone.getDefault()
        val now = Date()
        val seconds = TimeUnit.MILLISECONDS.toSeconds(now.time - past.time)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(now.time - past.time)
        val hours = TimeUnit.MILLISECONDS.toHours(now.time - past.time)
        val days = TimeUnit.MILLISECONDS.toDays(now.time - past.time)
        return when {
            seconds < 2 -> "$seconds second ago"
            seconds < 60 -> "$seconds seconds ago"
            minutes < 2 -> "$minutes minute ago"
            minutes < 60 -> "$minutes minutes ago"
            hours < 2 -> "$hours hour ago"
            hours < 24 -> "$hours hours ago"
            days < 2 -> "$days day ago"
            else -> "$days days ago"
        }
    }

    fun dateDurationLeft(date: String): String {
        val format = SimpleDateFormat(SERVER_TIME_FORMAT)
        format.timeZone = TimeZone.getTimeZone("UTC")
        val past = format.parse(date)
        val now = format.parse(format.format(Date()))
        val milliSec = past.time - now.time
        return milliSec.toString()
    }

}