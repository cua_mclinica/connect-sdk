package com.mclinica.connect.platform.sdk.data.model

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.RawValue

data class UsersConfiguration(
    @SerializedName("is_enabled") val isEnabled: Boolean,
    @SerializedName("user_types") val userTypes: List<UserTypes>
) {

    data class UserTypes (
        @SerializedName("user_type") val userType : String,
        @SerializedName("program_id") val programId : String,
        @SerializedName("is_enabled") val isEnabled : Boolean,
        @SerializedName("label") val label : String,
        @SerializedName("available_actions") val availableActions : List<String>,
        @SerializedName("allowed_user_segment_count") val allowedUserSegmentCount : Int,
        @SerializedName("tnc_link") val tncLink : String?,
        @SerializedName("privacy_policy_link") val privacyPolicyLink : String?,
        @SerializedName("user_guide_link") val userGuideLink : String?,
        @SerializedName("faq_link") val faqLink : String?,
        @SerializedName("metadata") val metadata : User.Metadata?,
        @SerializedName("created_at") val createdAt : String?,
        @SerializedName("updated_at") val updatedAt : String?,
        @SerializedName("deleted_at") val deletedAt : String?,
        @SerializedName("user_field_configurations") val userFieldConfigurations : List<UserFieldConfigurations>
    )

    data class UserFieldConfigurations (
        @SerializedName("program_id") val programId : String,
        @SerializedName("name") val name : String,
        @SerializedName("label") val label : String,
        @SerializedName("for_user_type") val forUserType : String,
        @SerializedName("type") val type : String,
        @SerializedName("title") val title : String,
        @SerializedName("hint") val hint : String?,
        @SerializedName("placeholder") val placeholder : String,
        @SerializedName("prefix") val prefix : String?,
        @SerializedName("is_required") val isRequired : Boolean,
        @SerializedName("is_visible") val isFieldVisible : Boolean,
        @SerializedName("is_default") val isDefault : Boolean,
        @SerializedName("regex_validations") val regexValidations : List<RegexValidation>?,
        @SerializedName("error_message") val errorMessage : String?,
        @SerializedName("field_type_options") val fieldTypeOptions : FieldTypeOptions?,
        @SerializedName("order") val order : Int,
        @SerializedName("created_at") val createdAt : String?,
        @SerializedName("updated_at") val updatedAt : String?,
        @SerializedName("deleted_at") val deletedAt : String?,
        @SerializedName("text") val text: String?,
        @SerializedName("enabled")val enabled : Boolean?
    )

    data class FieldConfiguration (
        @SerializedName("name") val name : String,
        @SerializedName("label") val label : String,
        @SerializedName("type") val type : String,
        @SerializedName("title") val title : String?,
        @SerializedName("hint") val hint : String?,
        @SerializedName("placeholder") val placeholder : String?,
        @SerializedName("prefix") val prefix : String?,
        @SerializedName("belongs_to") val belongsTo : String?,
        @SerializedName("for_user_type") val forUserType : String?,
        @SerializedName("is_required") val isRequired : Boolean,
        @SerializedName("is_visible") val isFieldVisible : Boolean,
        @SerializedName("is_default") val isDefault : Boolean,
        @SerializedName("regex_validations") val regexValidations : List<RegexValidation>?,
        @SerializedName("error_message") val errorMessage : String?,
        @SerializedName("field_type_options") val fieldTypeOptions : FieldTypeOptions?,
        @SerializedName("order") val order : Int
    )

    data class RegexValidation(
        @SerializedName("regex") val regex: String,
        @SerializedName("error_message") val errorMessage: String
    )

    data class FieldTypeOptions(
        @SerializedName("should_uppercase") val shouldUpperCase: Boolean?,
        @SerializedName("file_type_restrictions") val fileTypeRestrictions: List<String>?,
        @SerializedName("mobile_prefixes") val mobilePrefixes: List<MobilePrefixes>?,
        @SerializedName("select_options") val selectOptions: List<SelectOptions>?,
        @SerializedName("icon_empty_file") val iconEmptyFile: String?,
        @SerializedName("icon_selected_file") val iconSelectedFile: String?,
        @SerializedName("image_input_strategy") val imageInputStrategy: List<String>?,
        @SerializedName("select_dropdown_options") val selectDropDownOptions: List<SelectDropDownOptions>?
    )

    data class MobilePrefixes(
        @SerializedName("iso_code") val isoCode: String?,
        @SerializedName("calling_code") val callingCode: String?
    )

    data class SelectOptions(
        @SerializedName("label") val label: String?,
        @SerializedName("value") val data: @RawValue Any?,
        @SerializedName("image_url") var imageUrl: String,
        @SerializedName("branch_name") var branchName: String,
        @SerializedName("branch_notes") var branchNotes: String,
        @SerializedName("branch_address") var branchAddress: String,
        @SerializedName("branch_schedule") var branchSchedule: String,
        @SerializedName("hospital_filter") var hospitalFilter: List<String>?,
        @SerializedName("brand_schedule") var brandSchedule: String
    )

    data class SelectDropDownOptions(
        @SerializedName("label") val label: String?,
        @SerializedName("value") val value: String?,
        @SerializedName("image_url") val imageUrl: String?
    ) {
        override fun toString(): String {
            return label.toString()
        }
    }

}