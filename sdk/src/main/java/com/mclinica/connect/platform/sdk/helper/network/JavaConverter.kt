package com.mclinica.connect.platform.sdk.helper.network

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

object JavaConverter {

    var gson = Gson()

    @TypeConverter
    @JvmStatic
    fun stringToStringList(data: String?): List<String>? {
        if (data == null) {
            return emptyList()
        }
        val listType = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun stringListToString(items: List<String>): String? {
        return gson.toJson(items)
    }

    @TypeConverter
    @JvmStatic
    fun fromString(data: String?): ArrayList<String?>? {
        if (data == null) {
            return arrayListOf()
        }
        val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromArrayList(items: ArrayList<String?>?): String? {
        return gson.toJson(items)
    }

}