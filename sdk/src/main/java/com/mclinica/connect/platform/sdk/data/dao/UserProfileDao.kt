package com.mclinica.connect.platform.sdk.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mclinica.connect.platform.sdk.data.model.User
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface UserProfileDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(profile: User.Profile)

    @Query("SELECT * FROM user_profile_data")
    fun getUserProfile(): Flowable<User.Profile>

    @Query("SELECT * FROM user_profile_data")
    fun getUserProfileMaybe(): Maybe<User.Profile>

    @Query("DELETE FROM user_profile_data")
    fun deleteUserProfile()

}