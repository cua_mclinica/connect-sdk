package com.mclinica.connect.platform.sdk.helper.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri

object Utility {

    // count words in string. maybe we can use this in future projects.
    private const val OUT = 0
    private const val IN = 1

    fun countWords(msg: String): Int {
        var state = OUT
        var letters = 0
        var wordCount = 0

        while (letters < msg.length) {
            if (msg[letters] == ' ' || msg[letters] == '\n' || msg[letters] == '\t') {
                state = OUT
            } else if (state == OUT) {
                state = IN
                ++wordCount
            }
            ++letters
        }
        return wordCount
    }

    fun openPlayStore(context: Context, packageName: String?) {
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$packageName")
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }

}