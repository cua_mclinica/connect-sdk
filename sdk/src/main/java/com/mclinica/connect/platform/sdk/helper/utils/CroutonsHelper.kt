package com.mclinica.connect.platform.sdk.helper.utils

import android.app.Activity
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import com.mclinica.connect.platform.sdk.R
import de.keyboardsurfer.android.widget.crouton.Configuration
import de.keyboardsurfer.android.widget.crouton.Crouton
import de.keyboardsurfer.android.widget.crouton.Style

object CroutonsHelper {
    
    private fun style(style: Int = 1, duration: Int = 2500) = Style.Builder().apply {
        val config = Configuration.Builder().apply {
            setDuration(duration)
            setInAnimation(R.anim.crouton_in)
            setOutAnimation(R.anim.crouton_out)
            setPaddingInPixels(24)
        }.build()

        setBackgroundColor(
            when (style) {
                0 -> android.R.color.holo_red_light
                1 -> android.R.color.holo_green_light
                else -> 0
            }
        )
        setTextAppearance(R.style.CroutonsStyle)
        setConfiguration(config)
    }.build()

    fun show(activity: Activity, msg: String?, style: Int) {
        close()
        Crouton.makeText(activity, msg,
            style(
                style
            )
        ).show()
    }

    fun show(activity: Activity, @StringRes msg: Int, style: Int) {
        close()
        Crouton.makeText(activity, msg,
            style(
                style
            )
        ).show()
    }

    fun show(activity: Activity, msg: String?, style: Int, @IdRes view: Int) {
        close()
        Crouton.makeText(activity, msg,
            style(
                style
            ), view).show()
    }

    fun show(activity: Activity, @StringRes msg: Int, style: Int, @IdRes view: Int) {
        close()
        Crouton.makeText(activity, msg,
            style(
                style
            ), view).show()
    }

    fun show(activity: Activity, @StringRes msg: Int, style: Int, viewGroup: ViewGroup) {
        close()
        Crouton.makeText(activity, msg,
            style(
                style
            ), viewGroup).show()
    }

    fun show(activity: Activity, msg: String?, style: Int, viewGroup: ViewGroup) {
        close()
        Crouton.makeText(activity, msg,
            style(
                style
            ), viewGroup).show()
    }

    fun close() = Crouton.cancelAllCroutons()
}