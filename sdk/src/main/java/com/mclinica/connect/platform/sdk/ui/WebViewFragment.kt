package com.mclinica.connect.platform.sdk.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import com.mclinica.connect.platform.sdk.R
import com.mclinica.connect.platform.sdk.base.BaseFragment
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.helper.callback.FailCallback
import com.mclinica.connect.platform.sdk.helper.callback.SuccessCallback
import kotlinx.android.synthetic.main.fragment_web_view.*

class WebViewFragment : BaseFragment() {

    companion object {

        private const val ARG_TYPE: String = "arg_type"
        private const val ARG_LINK: String = "arg_link"

        const val TYPE_TERMS_AND_CONDITION: Int = 1
        const val TYPE_FAQ: Int = 2
        const val TYPE_USER_GUIDE: Int = 3
        const val TYPE_PRIVACY_POLICY: Int = 4

        fun newInstance(type: Int): WebViewFragment {
            val fragment = WebViewFragment()
            val arguments = Bundle()
            arguments.putInt(ARG_TYPE, type)
            fragment.arguments = arguments
            return fragment
        }

        fun newInstance(link: String): WebViewFragment {
            val fragment = WebViewFragment()
            val arguments = Bundle()
            arguments.putString(ARG_LINK, link)
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_web_view, container, false)
    }

    fun loadWebView(successCallback: SuccessCallback, failCallback: FailCallback) {
        webView.also {
            it.settings.javaScriptEnabled = true
            it.settings.setAppCacheEnabled(false)
            it.settings.cacheMode = WebSettings.LOAD_NO_CACHE
            it.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    successCallback.invoke()
                    super.onPageFinished(view, url)
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    return when {
                        url.contains("mailto:") -> {
                            view.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                            true
                        }
                        url.contains("tel:") -> {
                            view.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                            true
                        }
                        else -> {
                            view.loadUrl(url)
                            true
                        }
                    }
                }

                override fun onReceivedError(
                    view: WebView?,
                    request: WebResourceRequest?,
                    error: WebResourceError?
                ) {
                    failCallback.invoke()
                    super.onReceivedError(view, request, error)
                }
            }
        }

        val webLink: String = arguments?.getString(ARG_LINK, "")!!
        if (webLink.isNotBlank()) {
            webView.loadUrl(webLink)
        } else {
            when (arguments?.getInt(ARG_TYPE, -1)!!) {
                TYPE_TERMS_AND_CONDITION -> webView.loadUrl(getTermsAndConditions())
                TYPE_FAQ -> webView.loadUrl(getFAQ())
                TYPE_PRIVACY_POLICY -> webView.loadUrl(getPrivacyPolicy())
                TYPE_USER_GUIDE -> webView.loadUrl(getUserGuide())
            }
        }

    }

    private fun getTermsAndConditions(userType: String? = null): String? {
        val value = userType ?: LocalDisk.userProfile?.userType
        val type = LocalDisk.appConfiguration?.usersConfiguration?.userTypes?.first {
            it.userType == value
        }
        return type?.tncLink
    }

    private fun getFAQ(userType: String? = null): String? {
        val value = userType ?: LocalDisk.userProfile?.userType
        val type = LocalDisk.appConfiguration?.usersConfiguration?.userTypes?.first {
            it.userType == value
        }
        return type?.faqLink
    }

    private fun getPrivacyPolicy(userType: String? = null): String? {
        val value = userType ?: LocalDisk.userProfile?.userType
        val type = LocalDisk.appConfiguration?.usersConfiguration?.userTypes?.first {
            it.userType == value
        }
        return type?.privacyPolicyLink
    }

    private fun getUserGuide(): String? {
        val config = LocalDisk.appConfiguration
        return if (LocalDisk.isLoggedIn!!) {
            val userType = LocalDisk.userProfile?.userType
            val type = config?.usersConfiguration?.userTypes?.first { it.userType == userType }
            type?.userGuideLink
        } else {
            config?.programGuideLink
        }
    }

}