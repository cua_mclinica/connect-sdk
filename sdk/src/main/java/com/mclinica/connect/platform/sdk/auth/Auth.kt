package com.mclinica.connect.platform.sdk.auth

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.fragment.app.Fragment
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import net.openid.appauth.*

class Auth(
    context: Context,
    private val config: AuthenticationConfig? = null
) : AuthenticationService {

    private val authService: AuthorizationService by lazy { AuthorizationService(context) }

    companion object {

        const val RC_AUTH = 97
        private var auth: Auth? = null

        @Synchronized
        fun auth(
            context: Context,
            config: AuthenticationConfig
        ): Auth {
            if (auth == null) auth = Auth(context, config)
            return auth!!
        }

        @Synchronized
        fun auth(
            context: Context
        ): Auth {
            if (auth == null) auth = Auth(context)
            return auth!!
        }

        fun auth(): Auth {
            checkNotNull(auth) { "Auth can not be null" }
            return auth!!
        }
    }

    override fun connect(
        fragment: Fragment,
        failed: (AuthorizationException?) -> Unit,
        start: (() -> Unit)?,
        finished: (() -> Unit)?
    ) {
        start?.invoke()
        AuthorizationServiceConfiguration.fetchFromIssuer(Uri.parse(config?.issuer)) { serviceConfiguration, error ->
            if (serviceConfiguration != null) {
                authService.getAuthorizationRequestIntent(serviceConfiguration.configBuilder())
                    .also {
                        fragment.startActivityForResult(it, RC_AUTH)
                    }
                finished?.invoke()
            } else failed.invoke(error)
        }
    }

    override fun connect(
        activity: Activity,
        failed: (AuthorizationException?) -> Unit,
        start: (() -> Unit)?,
        finished: (() -> Unit)?
    ) {
        start?.invoke()
        AuthorizationServiceConfiguration.fetchFromIssuer(Uri.parse(config?.issuer)) { serviceConfiguration, error ->
            if (serviceConfiguration != null) {
                authService.getAuthorizationRequestIntent(serviceConfiguration.configBuilder())
                    .also {
                        activity.startActivityForResult(it, RC_AUTH)
                    }
                finished?.invoke()
            } else failed.invoke(error)
        }
    }


    override fun connect(
        context: Context,
        pendingIntent: PendingIntent,
        failed: (AuthorizationException?) -> Unit,
        start: (() -> Unit)?,
        finished: (() -> Unit)?
    ) {
        start?.invoke()
        AuthorizationServiceConfiguration.fetchFromIssuer(Uri.parse(config?.issuer)) { serviceConfiguration, error ->
            if (serviceConfiguration != null) {
                authService.performAuthorizationRequest(
                    serviceConfiguration.configBuilder(),
                    pendingIntent
                )
                finished?.invoke()
            } else failed.invoke(error)
        }
    }

    private fun AuthorizationServiceConfiguration.configBuilder(): AuthorizationRequest =
        AuthorizationRequest.Builder(
            this,
            config?.clientId ?: "",
            config?.responseType ?: "",
            Uri.parse(config?.redirectUri)
        ).apply {
            setResponseType(ResponseTypeValues.CODE)
            setScope(AuthorizationRequest.Scope.OPENID)
            setDisplay(AuthorizationRequest.Display.POPUP)
            setPrompt(AuthorizationRequest.Prompt.LOGIN)
        }.build()

    override fun retrieveToken(
        intent: Intent?,
        success: (() -> Unit)?,
        failed: ((AuthorizationException?) -> Unit)?
    ) {
        tokenGetter(intent, success, failed)
    }

    override fun retrieveToken(
        requestCode: Int,
        intent: Intent?,
        success: () -> Unit,
        failed: (AuthorizationException?) -> Unit
    ) {
        if (requestCode == RC_AUTH) {
            tokenGetter(intent, success, failed)
        }
    }

    private fun tokenGetter(
        intent: Intent?,
        success: (() -> Unit)? = null,
        failed: ((AuthorizationException?) -> Unit)? = null
    ) {
        checkNotNull(intent) { "Intent must be not null" }
        val authResponse = AuthorizationResponse.fromIntent(intent)
        val authError = AuthorizationException.fromIntent(intent)

        if (authResponse != null) {
            authService.performTokenRequest(authResponse.createTokenExchangeRequest()) { response, ex ->
                if (ex == null) {
                    val state = AuthState().also {
                        it.update(authResponse, authError)
                        it.update(response, authError)
                    }
                    LocalDisk.authToken = AuthToken(state)
                    AppLog.e("auth token: ${LocalDisk.authToken}")
                    success?.invoke()
                } else failed?.invoke(ex)
            }
        } else failed?.invoke(authError)
    }

}