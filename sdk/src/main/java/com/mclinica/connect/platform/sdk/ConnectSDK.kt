package com.mclinica.connect.platform.sdk

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.facebook.stetho.Stetho
import com.mclinica.connect.platform.sdk.data.LocalDisk
import com.mclinica.connect.platform.sdk.helper.network.NetworkUtil
import com.mclinica.connect.platform.sdk.helper.utils.AppLog
import com.mclinica.connect.platform.sdk.helper.utils.AppLog.Forest.plant

object ConnectSDK {

    lateinit var context: Context
    lateinit var REDIRECT_URL: String
    lateinit var API_BASE_URL: String
    lateinit var PROGRAM_ID: String
    lateinit var CURRENT_VERSION: String
    internal var ENABLE_LOGS: Boolean = false

    var networkConnectivity: MutableLiveData<Boolean> = MutableLiveData()

    @Synchronized
    fun initialize(
        context: Context,
        url: String,
        programId: String,
        currentVersion: String,
        enableLogs: Boolean = false
    ) {

        this.context = context
        LocalDisk.init(context)

        REDIRECT_URL = "${context?.packageName}.mcltech://callbacks/auth"
        API_BASE_URL = url
        PROGRAM_ID = programId
        CURRENT_VERSION = currentVersion
        ENABLE_LOGS = enableLogs

        if (enableLogs) {
            plant(AppLog.DebugTree())
            Stetho.initializeWithDefaults(context)
        }

        NetworkUtil.listenToNetworkAvailability(context)

    }

}