package com.mclinica.connect.platform.sdk.helper.network

import android.os.Build
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.mclinica.connect.platform.sdk.ConnectSDK
import com.mclinica.connect.platform.sdk.data.bus.BusEvent
import com.mclinica.connect.platform.sdk.data.bus.RxEventBus
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthNetworkInterceptor @Inject constructor(
    private val sharedPreferences: RxSharedPreferences,
    private val rxEventBus: RxEventBus,
    private val jwt: Boolean
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {

        val userAgent = "Android"
        val original = chain!!.request()

        // Add headers
        val requestBuilder = original.newBuilder()
            .header("Accept", "application/json")
            .header("Conference-Agent", userAgent)
            .header("X_DEVICE_TYPE", userAgent)
            .header("X_DEVICE_VERSION", Build.VERSION.SDK_INT.toString())
            .header("X_DEVICE_MODEL", Build.MODEL)
            .header("X_APP_IDENTIFIER", ConnectSDK.PROGRAM_ID)

        val request = requestBuilder.build()
        val response = chain.proceed(request)
        if (response.code() == 401) {
            rxEventBus.post(BusEvent.AuthenticationError())
        }
        return response
    }

}