package com.mclinica.connect.platform.sdk.auth

import com.google.gson.annotations.SerializedName
import com.mclinica.connect.platform.sdk.data.model.CustomersConfiguration
import com.mclinica.connect.platform.sdk.data.model.TransactionsConfiguration
import com.mclinica.connect.platform.sdk.data.model.UsersConfiguration

data class ProgramConfiguration(
    @SerializedName("program_id") val id: String,
    @SerializedName("program_name") val name: String,
    @SerializedName("program_code") val code: String,
    @SerializedName("program_type") val type: String,
    @SerializedName("countries") val countries: List<String>,
    @SerializedName("languages") val languages: List<String>,
    @SerializedName("allowed_program_users_count") val allowedProgramUsersCount: Int,
    @SerializedName("start_date") val startDate: String,
    @SerializedName("end_date") val endDate: String,
    @SerializedName("program_guide_link") val programGuideLink: String?,
    @SerializedName("product_selection_type") val productSelectionType: String?,
    @SerializedName("program_metadata") val metadata: Any?,
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("updated_at") val updatedAt: String?,
    @SerializedName("deleted_at") val deletedAt: String?,
    @SerializedName("config_hash") val hash: String,
    @SerializedName("keycloak_connect_configuration") val keycloakConfig: KeyCloakConfiguration,
    @SerializedName("users") val usersConfiguration: UsersConfiguration,
    @SerializedName("customers") val customersConfiguration: CustomersConfiguration,
    @SerializedName("transactions") val transactionsConfiguration: TransactionsConfiguration
)