package com.mclinica.connect.platform.sdk.data

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


/**
 * Handles cache persistence using shared preference.
 */
class LocalDiskCacheBox(context: Context) {

    val sharedPreferences: SharedPreferences by lazy {
        context.getSharedPreferences(
            "connect_platform",
            Context.MODE_PRIVATE
        )
    }
    val converter by lazy { Gson() }

    inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }

    operator fun <T : Any> set(key: String, value: T?) {
        when (value) {
            is String? -> sharedPreferences.edit { it.putString(key, value) }
            is Int -> sharedPreferences.edit { it.putInt(key, value) }
            is Boolean -> sharedPreferences.edit { it.putBoolean(key, value) }
            is Float -> sharedPreferences.edit { it.putFloat(key, value) }
            is Long -> sharedPreferences.edit { it.putLong(key, value) }
            else -> sharedPreferences.edit {
                val typeToken = object : TypeToken<T>() {}.type
                it.putString(key, converter.toJson(value, typeToken))
            }
        }
    }

    inline operator fun <reified T : Any> get(key: String, defaultValue: T? = null): T? {
        return when (T::class) {
            String::class -> sharedPreferences.getString(key, defaultValue as? String) as T?
            Int::class -> sharedPreferences.getInt(key, defaultValue as? Int ?: -1) as T?
            Boolean::class -> sharedPreferences.getBoolean(
                key,
                defaultValue as? Boolean ?: false
            ) as T?
            Float::class -> sharedPreferences.getFloat(key, defaultValue as? Float ?: -1f) as T?
            Long::class -> sharedPreferences.getLong(key, defaultValue as? Long ?: -1) as T?
            else -> converter.fromJson(sharedPreferences.getString(key, null), T::class.java) as T?
        }
    }

    fun has(key: String) = sharedPreferences.contains(key)

    fun remove(key: String) = sharedPreferences.edit { it.remove(key).apply(); }
}