package com.mclinica.connect.platform.sdk.auth

import com.google.gson.annotations.SerializedName
import com.mclinica.connect.platform.sdk.data.model.RegisterResponse
import net.openid.appauth.AuthState

open class AuthToken(
    @SerializedName("access_token") val accessToken: String?,
    @SerializedName("refresh_token") val refreshToken: String?,
    @SerializedName("refresh_expires_in") val refreshExpiresIn: Long?
) {

    constructor(authState: AuthState) : this(
        authState.accessToken,
        authState.refreshToken,
        authState.lastTokenResponse?.additionalParameters?.get("refresh_expires_in")?.toLong()
    )

    constructor(registerResponse: RegisterResponse) : this(
        registerResponse.accessToken,
        registerResponse.refreshToken,
        registerResponse.refreshExpiresIn
    )
}